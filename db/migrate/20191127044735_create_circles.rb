class CreateCircles < ActiveRecord::Migration[5.2]
  def change
    create_table :circles do |t|
      t.string :uni_name
      t.string :id
      t.string :email
      t.string :pass
      t.string :circle_name
      t.string :top_name

      t.timestamps
    end
  end
end
